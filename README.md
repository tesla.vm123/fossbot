# FossBot

FossBot is a Java Discord Bot for multiple purposes.

## Installation

You need [Java](https://adoptopenjdk.net/) installed in order to run the bot.


```bash
java -jar fossbot.jar
```

## Usage

After running the bot for the first time it will generate a config.properties file in which you will net to set your bot token in order for the bot to run.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
