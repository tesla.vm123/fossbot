/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import commands.*;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;

import java.io.*;
import java.util.Properties;


/**
 * @author Kamey
 * @author a8_
 */
public class main {

    public static void main(String[] args) throws Exception {

        boolean ready = true;


        //Load token from config
        File configFile = new File("config.properties");
        String token = ("t");

        try {
            FileReader reader = new FileReader(configFile);
            Properties props = new Properties();
            props.load(reader);
            token = props.getProperty("token");
            reader.close();
        } catch (FileNotFoundException ex) {
            //stop bot from trying to run
            ready = false;
            //if file does not exist we will create file
            File config = new File("config.properties");


            //if below is not "true" it means there was a warning
            if(!config.createNewFile()) {
                System.out.println("There was an issue");
            }
            System.out.println("Created new file" + config.getAbsolutePath());
            System.out.println("Please set bot token in newly created config file");
            //we are now writeing default configs to file
            FileWriter myWriter = new FileWriter("config.properties");
            myWriter.write("#Fossbot discord bot config file");
            //windows handles new lines diffrently so we have java choose what to do for new line
            myWriter.write(System.lineSeparator());
            //this writes to the files otherwise if we did another write it would overwrite first one
            myWriter.flush();
            myWriter.write("token=PutYourBotTokenHere");
            myWriter.write("mute_role_id=PutYourRoleIdHere");
            myWriter.close();
        } catch (IOException ioerror) {
            System.out.println("I/O ERROR");
            ioerror.printStackTrace();
            //stop bot from trying to run
            ready = false;
        }


        if (ready) {

            JDA jda = JDABuilder.createDefault(token).build();


            jda.getPresence().setActivity(Activity.playing("?help for help"));

            jda.addEventListener(new calculator());
            jda.addEventListener(new eight_ball());
            jda.addEventListener(new user());
            jda.addEventListener(new invite());
            jda.addEventListener(new inspire());
            jda.addEventListener(new info());
            jda.addEventListener(new help());
            jda.addEventListener(new invite());
            jda.addEventListener(new chuck());
            jda.addEventListener(new boobs());
            jda.addEventListener(new meme());
            jda.addEventListener(new randomporn());
            jda.addEventListener(new randomgay());
            jda.addEventListener(new psize());
            jda.addEventListener(new hentai());
            jda.addEventListener(new mcskin());
            jda.addEventListener(new mcsteal());
            jda.addEventListener(new avatar());
            jda.addEventListener(new server());
            jda.addEventListener(new mute());

        } else {
            System.out.println("Something went wrong bot will not start");
        }


    }


}