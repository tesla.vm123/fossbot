/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import parsers.meme_json;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.IOException;
import java.util.Objects;

public class meme extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String messageSent = event.getMessage().getContentRaw();



        if (messageSent.contains("?meme")) {


            if (!Objects.requireNonNull(event.getMember()).getUser().isBot()) {
                EmbedBuilder Embed1 = new EmbedBuilder();
                String[] responce = {"error!"};
                meme_json memegetter = new meme_json();

                String word;

                try {
                    word = messageSent.substring(messageSent.indexOf("?meme") + 6);
                } catch (IndexOutOfBoundsException e) {
                    word = ("");
                }

                try {
                    responce = memegetter.getmeme("https://meme-api.herokuapp.com/gimme/" + word);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Embed1.setImage(responce[0]);
                Embed1.setTitle(responce[1], responce[2]);

                event.getChannel().sendMessage(Embed1.build()).queue();


            }

        }



    }
}
