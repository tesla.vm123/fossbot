/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author a8_
 */
// https://gitlab.com/a8_

public class calculator extends ListenerAdapter {


    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {

        String[] message = event.getMessage().getContentRaw().split(" ");
        if(message[0].contains("?calc")){

            String msg = event.getMessage().getContentDisplay();
            String[] separated = msg.split("calc");
            // separated[0]; // this will contain "?calc"
            //separated[1]; //this will contain after
            //separated[1] = separated[1].trim();
//            separated[1] = separated[1].replaceAll("\\s+","");
//           long lelong = Long.parseLong(separated[1]);
//
//            event.getChannel().sendMessage("The Result is: " + String.valueOf(lelong) ).queue();

            //try js
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            try {
                event.getChannel().sendMessage("The Result is: " + engine.eval(separated[1]) ).queue();
            } catch (ScriptException e) {
                e.printStackTrace();
            }



        }

    }

}