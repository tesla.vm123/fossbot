/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;

public class invite extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        int timeString = 1; //seconds in an hour
        EmbedBuilder Embed00 = new EmbedBuilder();
        Embed00.setColor(Color.green);
        String[] message = event.getMessage().getContentRaw().split(" "); //splits message received into an array holding each word in each index
        if (message.length == 1 && message[0].equalsIgnoreCase("?invite")){  //if they type the command alone, tell them how to use it
            Embed00.setTitle("To use ?invite command do: ?invite create");
            event.getChannel().sendMessage(Embed00.build()).queue(); //tells user how to use the $invite command
        }else if(message[0].equalsIgnoreCase("?invite") && message[1].equalsIgnoreCase("create")) {
            Embed00.setTitle("Invite:");
            Embed00.setDescription("Hi " + event.getAuthor().getName() + "! If you want to invite someone? Cool!\nGive them this url: " + event.getChannel().createInvite().setMaxAge(timeString).complete().getUrl() + "\nThe invite url will expire in: " + timeString + " hour.");
            event.getChannel().sendMessage(Embed00.build()).queue(); //Tells them how long the invite will exist for
        }

    }
}