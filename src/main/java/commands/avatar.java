/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class avatar extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String messageSent = event.getMessage().getContentRaw();


        if (messageSent.equalsIgnoreCase("?avatar")) {
            EmbedBuilder Embed1 = new EmbedBuilder();
            Embed1.setImage(event.getAuthor().getAvatarUrl());
            Embed1.setTitle("Download", event.getAuthor().getAvatarUrl());
            event.getChannel().sendMessage(Embed1.build()).queue();
        }


    }
}
