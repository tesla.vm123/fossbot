/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import parsers.mojang_json;

import java.io.IOException;
import java.util.Objects;

public class mcsteal extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String messageSent = event.getMessage().getContentRaw();


        if (messageSent.contains("?mcsteal")) {
            if (!Objects.requireNonNull(event.getMember()).getUser().isBot()) {
                String responce2 = "placeholder";
                mojang_json mojanggetter = new mojang_json();
                String word = messageSent.substring(messageSent.indexOf("?mcskin") + 10);
                word = word.trim();
                try {
                    responce2 = mojanggetter.getmojang("https://api.mojang.com/users/profiles/minecraft/" + word);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                event.getChannel().sendMessage("  https://crafatar.com/skins/" + responce2).queue();
            }

        }


    }
}
