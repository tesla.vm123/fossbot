/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;




/**
 *
 * @author Kamey
 * @author a8_
 */

public class mute extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {


        String[] args = event.getMessage().getContentRaw().split("\\s+");
        EmbedBuilder mute = new EmbedBuilder();
        mute.setColor(Color.green);
        if (args[0].equalsIgnoreCase("?mute")) {
            if (!event.getMessage().getMentionedMembers().isEmpty()) {
                Member member = event.getMessage().getMentionedMembers().get(0);
                String name = event.getAuthor().getName();
                if (args.length == 2) {
                    Role role = event.getGuild().getRoleById("786345884593160243");
                    assert member != null;
                    assert role != null;

                    if (!member.getRoles().contains(role)) {
                        // Mute
                        mute.setTitle("Muted " + name + ".");
                        event.getChannel().sendMessage(mute.build()).queue();
                        event.getGuild().addRoleToMember(member, role).complete();
                    } else {
                        // Unmute
                        mute.setTitle("Unmuted " + name + ".");
                        event.getChannel().sendMessage(mute.build()).queue();
                        event.getGuild().removeRoleFromMember(member, role).complete();
                    }
                }
                else if (args.length == 3) {
                        Role role = event.getGuild().getRoleById("786345884593160243");

                        mute.setTitle("Muted " + name + " for " + args[2] + " minutes.");
                        event.getChannel().sendMessage(mute.build()).queue();
                        assert member != null;
                        assert role != null;
                        event.getGuild().addRoleToMember(member, role).complete();

                        // Remove mute for couple of seconds.
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                                    @Override
                                    public void run() {
                                        mute.setTitle("Unmuted " + name + ".");
                                        event.getChannel().sendMessage(mute.build()).queue();
                                        event.getGuild().removeRoleFromMember(member, role).complete();
                                    }
                                },
                                Integer.parseInt(args[2]) * 100000
                        );
                } else {
                    mute.setTitle("Incorrect syntax. Use `?mute [user mention] [time {optional}]`");
                    event.getChannel().sendMessage(mute.build()).queue();
                }

            }
        }
    }
}

