/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import parsers.server_json;

import java.io.IOException;
import java.util.Objects;

public class server extends ListenerAdapter {

    //to keep Random truly random we need to leave it running in the background.
    //static Random rnd = new Random(); commented out as its never used

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {


        String messageSent = event.getMessage().getContentRaw();


        if (messageSent.contains("?server")) {


            if (!Objects.requireNonNull(event.getMember()).getUser().isBot()) {
                EmbedBuilder Embed1 = new EmbedBuilder();
                String[] responce = {"error!"};
                server_json servergetter = new server_json();

                String word;

                try {
                    word = messageSent.substring(messageSent.indexOf("?server") + 8);
                } catch (IndexOutOfBoundsException e) {
                    word = ("");
                }

                try {
                    responce = servergetter.getserver("https://mcapi.xdefcon.com/server/" + word + "/full/json");
                } catch (IOException e) {
                e.printStackTrace();
             }


                Embed1.setTitle("Minecraft server: " + word);
                Embed1.setThumbnail("https://api.minetools.eu/favicon/" + word + "/25565");
                Embed1.setDescription("**Status: " + responce[0] +"**" + "\n**Version: " + responce[1] + "**" + "\n**Players: " + responce[2] + "/" + responce[3] + "**");
                event.getChannel().sendMessage(Embed1.build()).queue();


            }

        }


    }
}
