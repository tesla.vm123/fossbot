/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;


public class user extends ListenerAdapter {



    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

             SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = new Date();

        // this is supposed to split message by spaces so a8_ don't remove space it's not mistaken.
        String[] message = event.getMessage().getContentRaw().split(" ");

        //command
        // will try to find when command ?user is typed and check check if at word 2 is user name.
        // and yea i figured out how to check for words not letters.
        // found a lot of these prefixes and embed and other JDA commands on stackoverflow and an chinese website tnx to them.
         if (message.length == 1 && message[0].equalsIgnoreCase("?user")){
            event.getChannel().sendMessage("To get a users info, type ?user [name]").queue();

         }else if(message.length == 2 && message[0].equalsIgnoreCase("?user")){
            String Name = message[1];


              User user1 = event.getGuild().getMembersByName(Name, true).get(0).getUser();
              String avatar = event.getGuild().getMembersByName(Name, true).get(0).getUser().getAvatarUrl();




            //here the embed stuff commands colors and other stuff i've added
            EmbedBuilder Embed = new EmbedBuilder();


             Embed.setTitle(Name + "'s Info:");
            Embed.setColor(Color.GREEN);
            Embed.addField("Name", user1.getName(), true);
             Embed.addField("Status", event.getGuild().getMembersByName(Name, true).get(0).getOnlineStatus().toString(), true);
            Embed.addField("Avatar:", "" + Objects.requireNonNull(event.getMember()).getAsMention(), true);
             Embed.setImage(avatar);
            Embed.setFooter("Request made at:  " + formatter.format(date), event.getGuild().getIconUrl());

            event.getChannel().sendMessage(Embed.build()).queue();


        }
    }



}