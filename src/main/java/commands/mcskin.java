/*
 *     This file is part of FossBot
 *     Copyright (C) 2020  Kamey, a8_
 *
 *     FossBot is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import parsers.mojang_json;

import java.io.IOException;
import java.util.Objects;

public class mcskin extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String messageSent = event.getMessage().getContentRaw();

        if (messageSent.contains("?mcskin")) {
            if (!Objects.requireNonNull(event.getMember()).getUser().isBot()) {
                EmbedBuilder Embed1 = new EmbedBuilder();
                Embed1.setTitle("Your Minecraft Skin:");
                String responce2 = "error!";
                mojang_json mojanggetter = new mojang_json();
                String word = messageSent.substring(messageSent.indexOf("?mcskin") + 8);
                word = word.trim();
                try {
                    responce2 = mojanggetter.getmojang("https://api.mojang.com/users/profiles/minecraft/" + word);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Embed1.setImage("https://visage.surgeplay.com/full/" + responce2);
                event.getChannel().sendMessage(Embed1.build()).queue();
            }

        }


    }
}
